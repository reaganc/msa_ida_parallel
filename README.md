# Collapse analyses using OpenSees
This repository contains Tcl scripts to estimate structural collapse capacity by conducting incremental dynamic analysis (IDA) or multiple stripe analysis using the central difference time integration scheme and multiple processors for increased efficiency.


## Summary of features

* Automatically create a two-dimensional, concentrated plastic hinge model of a steel moment frame building, given an input file containing frame dimensions, story loads, member dimensions and cross-section properties, etc.
    * Is currently set up to create a model of the 9-story, steel moment frame building designed in Seattle as part of the SAC steel project
    * Plastic hinges follow the Ibarra-Medina-Krawinkler bilinear hysteretic model, that incorporates in-cycle and cyclic deterioration of strength and stiffness
    * Shear deformation of the panel zones is modeled using a trilinear backbone curve
    * Can model basement levels, and RBS and cover-plate beam connections
    * Models the destabilizing effect of the adjacent gravity frame using a pin-connected leaning column
    * Small masses are added at all rotational degrees of freedom to allow analysis using the robust central difference time integration scheme
    * (Can be easily substituted by any other structural model instead)
* Run an elastic eigenvalue analysis
    * Obtains the lowest elastic modal period to determine the maximum allowable time step that can be used in conjunction with the central difference time integration scheme
* Run a nonlinear, static pushover analysis
* Run an incremental dynamic analysis (IDA)
    * Uses the central difference time integration scheme for increased robustness
    * Load-balanced parallel algorithm utilizes multiple processors, wherein one master processor directs the analysis, and many slave processors run different ground motions at different scale factors
    * Can resume an interrupted analysis
* Run a multiple stripe analysis
    * Uses the central difference time integration scheme for increased robustness
    * Utilizes multiple processors to run different ground motions at different intensity levels, simultaneously
* Python scripts to process the results of each type of analysis are included


## Requirements

* Running IDA requires OpenSeesMP and at least two processors (one master and at least one slave processor)
    * For best efficiency, # processors = 10*(# ground motions) + 1
* Multiple stripe analysis can be run using any number of processors
    * For best efficiency, # processors = (# intensity levels) * (# ground motions per intensity level)


## Help

For help and queries, feel free to send me a message on Bitbucket. To report bugs or request features, you can open an issue in the issue tracker.
