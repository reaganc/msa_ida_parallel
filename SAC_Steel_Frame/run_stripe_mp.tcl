##############################################################################################################
# Reagan Chandramohan                                                                                        #
# John A. Blume Earthquake Engineering Center                                                                #
# Stanford University                                                                                        #
# Last edited: 02-Jun-2015
##############################################################################################################

# Run a stripe analysis using multiple cores.
# For best results, run with as many processors as there are analyses to be run.

##############################################################################################################

# Initialize the total number of processors and the id of this processor
set numprocs [getNP]
set this_procid [getPID]

# Source the required files
source constants_units_kip_in.tcl
source frame_data.tcl
source create_steel_mf_model.tcl
source max_drift_check.tcl

# Initialize the list of ground motion folders to be run
set gmset SAC_Seattle
set inpath ../Ground_Motions/$gmset
set inpath_length [string length $inpath]
set indirlist [lsort [glob -directory $inpath -type d *]]

# Define the output path
set outpath Analysis_Results/Stripe/$gmset

# Create the list of ground motions to be run by looping over all the ground motion folders
set serial 0
foreach indir $indirlist {

    # Parse the input directory from the input path
    set indir_length [string length $indir]
    set gmdir [string range $indir [expr {$inpath_length + 1}] [expr {$indir_length - 1}]]

    # Import information about each ground motion in the GMInfo.txt file and add the information to the
    # "gminfo_dict" dictionary
    set gminfofile [open $indir/GMInfo.txt r]
    while {[gets $gminfofile line] >= 0} {
        
        # Read the filename and dt
        set filename [lindex $line 1]
        set dt [lindex $line 2]

        # Count the number of points
        set numpts 0
        set gmfile [open $indir/$filename r]
        while {[gets $gmfile line] >= 0} {
            incr numpts
        }
        close $gmfile

        # Add the ground motion information to "gminfo_dict"
        dict set gminfo_dict $serial indir $indir
        dict set gminfo_dict $serial gmdir $gmdir
        dict set gminfo_dict $serial filename $filename
        dict set gminfo_dict $serial dt $dt
        dict set gminfo_dict $serial numpts $numpts

        incr serial
    }
    close $gminfofile
}

# Define the collapse peak story drift and the nodes used to compute story drifts
set col_drift 0.10
set ctrl_nodes 1[format "%02d" [expr {$num_bays + 1}]]002
for {set story 1} {$story <= $num_stories} {incr story} {
    lappend ctrl_nodes 1[format "%02d" [expr {$num_bays + 1}]][format "%02d" $story]4
}

# Loop over all ground motion files and run the analyses for this processor
dict for {serial gminfo} $gminfo_dict {
    if {[expr {$serial % $numprocs}] == $this_procid} {
        dict with gminfo {

            # Define and create the output directory
            set filename_length [string length $filename]
            set outfilename [string range $filename 0 [expr {$filename_length - 4}]]
            set outdir $outpath/$gmdir/$outfilename
            file mkdir $outdir

            # Run the analysis
            set stripe_file [open $outdir/stripe.txt w]
            wipe
            CreateSteelMFModel frame_data.tcl
            source recorders_analysis_stripe.tcl
            close $stripe_file
        }

        # Display the status of the analysis
        set time [clock seconds]
        puts "Processor [expr {$this_procid + 1}]: Ground Motion #[expr {$serial + 1}] complete  -  \
                [clock format $time -format {%D %H:%M:%S}]"
    }
}
