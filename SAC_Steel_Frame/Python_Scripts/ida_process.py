# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import os
import shelve, contextlib
import scipy.stats, scipy.interpolate

# Define directories
gmdir = 'FEMA_P695_Far_Field'
inpath_gm = '../../Ground_Motions/{}'.format(gmdir)
inpath_ida = '../Analysis_Results/IDA/{}'.format(gmdir)

# Define parameters
col_drift = 0.10

# Read the elastic fundamental mode period
with open('../Model_Info/periods.out') as f:
    period = float(f.readline())

# Create a list of ground motion names
with open(os.path.join(inpath_gm, 'GMInfo.txt')) as infile:
    gmnames = np.array([line.split()[1].rstrip('.th') for line in infile])

# Read the IDA curves
sat1 = []
drift = []
sat1_coll = []

for gmname in gmnames:
    cur_sat1, cur_drift = np.loadtxt(os.path.join(inpath_ida, gmname, 'ida_curve.txt'), unpack=True)
    sat1.append(cur_sat1)
    drift.append(cur_drift)
    sat1_coll.append(cur_sat1[-1])

sat1_coll = np.array(sat1_coll)

# Compute the geometric mean IDA curve
drift_gmean = np.linspace(0, col_drift)
sat1_interp = np.array([scipy.interpolate.interp1d(np.hstack((drift_gm, col_drift)), \
        np.hstack((sat1_gm, sat1_gm[-1])))(drift_gmean) for drift_gm, sat1_gm in zip(drift, sat1)])
sat1_gmean = scipy.stats.gmean(sat1_interp, axis=0)

# Plot the IDA curves
factor = 1.1
ylim = (0, max(sat1_coll)*factor)
for drift_gm, sat1_gm in zip(drift, sat1):
    hand = plt.plot(np.hstack((drift_gm, col_drift)), np.hstack((sat1_gm, sat1_gm[-1])), 'b', alpha=0.5, \
        zorder=0)
hand += plt.plot(drift_gmean, sat1_gmean, 'k', lw=2, zorder=1)
plt.grid(True)
plt.xlim((0, col_drift))
plt.ylim(ylim)
plt.xlabel('Peak SDR ($rad$)', fontsize='x-large')
plt.ylabel('$S_a({:.2f}s)$ ($g$)'.format(period), fontsize='x-large')
plt.legend(hand, ('IDA curves', 'Geometric mean IDA curve'), loc='upper left')
plt.tight_layout()
plt.savefig(os.path.join(inpath_ida, 'IDA_Curves.png'))
plt.show()
plt.close()

# Compute the collapse fragility curve
collcap = {}
collcap['lnsig'], _, collcap['med'] = scipy.stats.lognorm.fit(sat1_coll, floc=0)

# Plot the collapse fragility curve
factor = 2.0
xlim = (min(sat1_coll)/factor, max(sat1_coll)*factor)
x_plot = np.logspace(np.log10(xlim[0]), np.log10(xlim[1]))
y_plot = scipy.stats.lognorm(collcap['lnsig'], scale=collcap['med']).cdf(x_plot)
plt.semilogx(x_plot, y_plot, 'b', lw=2)
plt.scatter(sorted(sat1_coll), np.arange(1, len(sat1_coll) + 1, dtype=float)/len(sat1_coll), s=2, color='k')

plt.yticks(np.linspace(0, 1, 11))
plt.grid(True, which='both')
plt.xlim(xlim)
plt.ylim((0, 1))
plt.xlabel('$S_a({:.2f}s)$ ($g$)'.format(period), fontsize='x-large')
plt.ylabel('Probability of Collapse', fontsize='x-large')
plt.title(r'$\mu$: {:.2f}g, $\beta$: {:.2f}'.format(collcap['med'], collcap['lnsig']), fontsize='x-large')
plt.tight_layout()
plt.savefig(os.path.join(inpath_ida, 'Collapse_Fragility.png'))
plt.show()
plt.close()

# Save data
with contextlib.closing(shelve.open(os.path.join(inpath_ida, 'IDA_Results.dat'))) as outfile:
    outfile['sat1'] = sat1
    outfile['drift'] = drift
    outfile['sat1_coll'] = sat1_coll
    outfile['collcap'] = collcap
    outfile['sat1_gmean'] = sat1_gmean
    outfile['drift_gmean'] = drift_gmean
