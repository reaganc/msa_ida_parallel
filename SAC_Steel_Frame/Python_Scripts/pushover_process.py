# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import os
import shelve, contextlib

# Define directories
path = '../Analysis_Results/Pushover'

# Read the pushover analysis results
base_shear, roof_drift = np.loadtxt(os.path.join(path, 'roof_drift.out'), unpack=True)

# Plot the pushover curve
plt.plot(roof_drift, base_shear)
plt.grid()
plt.xlim(xmin=0)
plt.ylim(ymin=0)
plt.xlabel('Roof drift ($rad$)', fontsize='x-large')
plt.ylabel('Base shear ($kip$)', fontsize='x-large')
plt.tight_layout()
plt.savefig(os.path.join(path, 'Pushover_Curve.png'))
plt.show()
plt.close()
